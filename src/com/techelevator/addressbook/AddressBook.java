package com.techelevator.addressbook;

import java.util.ArrayList;
import java.util.List;

public class AddressBook {
	private List<Contact> myAddressBook; 
	
	public AddressBook() {	
		myAddressBook = new ArrayList<Contact>();
	}
	
	public void add(Contact newContact) {
		myAddressBook.add(newContact);
	}
	
	public Contact find(String name){
		for(Contact contact : myAddressBook ) {
			if(contact.getName().startsWith(name)){
				return contact;
			}
			
		}return null;
	}
	
	public List<Contact> getAllContacts(){
		return myAddressBook;
	}
}