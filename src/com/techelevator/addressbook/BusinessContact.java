package com.techelevator.addressbook;

public class BusinessContact implements Contact {
	
	private String businessName;
	private String businessAddress;
	
	public BusinessContact(String businessName, String businessAddress){
		this.businessName = businessName;
		this.businessAddress = businessAddress;
	}
	
	public String getName() {
		return this.businessName;
	}

	public String getAddress() {
		return this.businessAddress;
	}
	
	public String getBusinessName(){
		return this.businessName;
	}
}


