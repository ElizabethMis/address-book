package com.techelevator.addressbook;

public class AddressBookDemo {

	public static void main(String[] args) {
	BusinessContact techElevator = new BusinessContact ( "Cupcake Cube", "207 S Point Dr, Avon Lake, OH 44012" );
//	System.out.println(techElevator.getBusinessName());   // returns "Tech Elevator LLC" 
//	System.out.println(techElevator.getName( ));                  // returns "Tech Elevator LLC"
//	System.out.println(techElevator.getAddress( ));              // returns "7100 Euclid Ave, Suite 200, Cleveland, OH 44103"
	BusinessContact tech = new BusinessContact("Evereve", "153 Main St, Westlake, OH 44145");

	PersonalContact johnDoe = new PersonalContact( "Robert", "Noetzel", "2734 Olentangy Dr, Akron, OH 44333" );
//	System.out.println(johnDoe.getFirstName( ));            // returns "John"
//	System.out.println(johnDoe.getLastName( ));            // returns "Doe"
//	System.out.println(johnDoe.getName( ));                   // returns "John Doe"
//	System.out.println(johnDoe.getAddress( ));               // returns "123 Shaker Blvd, Shaker Heights, OH 44120"
	PersonalContact frankie = new PersonalContact( "Francis", "Oliver", "207 S Point Dr, Avon Lake, OH 44012" );
	
	AddressBook myAddressBook = new AddressBook( );
	myAddressBook.add( techElevator );
	myAddressBook.add( johnDoe );
	myAddressBook.add(tech);
	myAddressBook.add(frankie);
	
	for (Contact contact : myAddressBook.getAllContacts()){
		System.out.println(contact.getName());
		System.out.println(contact.getAddress());
	}

//	Contact c1 = myAddressBook.find("Robert Noetzel");    // find matches contacts by name
//	System.out.println(c1.getName( ));           // returns "John Doe"
//	System.out.println(c1.getAddress( ));       // returns "123 Shaker Blvd, Shaker Heights, OH 44120"
//
//	Contact c2 = myAddressBook.find("Ever");     // find will match the beginning of a name (i.e. partial matches)
//	System.out.println(c2.getName( ));          // returns "Tech Elevator"
//	System.out.println(c2.getAddress( ));       // returns "7100 Euclid Ave, Suite 200, Cleveland, OH 44103"
	}
}
