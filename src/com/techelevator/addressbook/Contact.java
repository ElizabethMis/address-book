package com.techelevator.addressbook;

public interface Contact {

	public String getName();
	public String getAddress();
}
